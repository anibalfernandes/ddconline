import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { CdkTableModule } from '@angular/cdk/table';
import 'hammerjs';
import { PersonalDetailsService } from './sections/personal-details/services/personal-details.service';
import { ConsultantsService } from './sections/consultants/services/consultants.service';
import { ConsultantListService } from './sections/consultant-list/services/consultant-list.service';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatStepperModule,
} from '@angular/material';

// SERVICES
import { ProfileService } from './modules/profile/services/profile.service';
import { ProfileBackend } from './modules/profile/services/profile.backend';

/// SECTIONS
import { FooterSection } from './sections/footer/footer.section';
import { PersonalDetailsSection } from './sections/personal-details/personal-details.section';
import { ConsultantsSection } from './sections/consultants/consultants.section';
import { ConsultantListSection } from './sections/consultant-list/consultant-list.section';
import { TechnologyDetailsSection } from './sections/technology-details/technology-details.section';

/// COMPONENTS
import { InputText } from './components/input-text/input-text.component';
import { InputEmail } from './components/input-email/input-email.component';
import { InputSelect } from './components/input-select/input-select.component';
import { WizardComponent } from './components/wizard/wizard.component';
import { TechnologyDetailsService } from './sections/technology-details/services/technology-details.service';

const appRoutes: Routes = [
  {
    path: '',
    component: PersonalDetailsSection
  },
  {
    path: 'technologies',
    component: TechnologyDetailsSection
  },
  {
    path: 'consultant/:consultantId',
    component: ConsultantsSection
  },
  {
    path: 'consultantlist',
    component: ConsultantListSection
  }
];

@NgModule({
  exports: [
    CdkTableModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
  ]
})
export class MaterialModule { }

@NgModule({
  declarations: [
    PersonalDetailsSection,
    TechnologyDetailsSection,
    FooterSection, 
    ConsultantsSection,
    ConsultantListSection,
    WizardComponent,
    AppComponent,
    InputText,
    InputEmail,
    InputSelect
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    MaterialModule,
    MatNativeDateModule,
    ReactiveFormsModule,
  ],
  providers: [
    ProfileService,
    ProfileBackend,
    PersonalDetailsService,
    TechnologyDetailsService,
    ConsultantsService,
    ConsultantListService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
