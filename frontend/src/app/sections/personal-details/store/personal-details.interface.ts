export interface PersonalDetails {
  "academies": any[];
  "certifications": any[];
  "courses": any[];
  "languages": any[];
  "name": any;
  "surname": any;
  "birthDate": string;
  "phoneNumber": any;
  "email": any;
  "nationality": any;
  "role": any;
  "description": any;
}
