import { Component } from '@angular/core';
import { Injectable } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { NgForm } from '@angular/forms';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { PersonalDetails } from './store/personal-details.interface';
import { PersonalDetailsService } from './services/personal-details.service';

@Component({
  selector: 'personal-details-section',
  templateUrl: 'personal-details.section.html',
  styleUrls: ['personal-details.section.scss']
})

export class PersonalDetailsSection {
  private url: string = "http://localhost:8080/consultants";
  private langId: number = 1;
  private academyId: number = 1;
  private certificateId: number = 1;
  private courseId: number = 1;

  constructor(private http: HttpClient,
    private personalDetailsService: PersonalDetailsService) { }

  submitted = false;

  private "addLanguages" = [
    {
      "id": 0,
    }
  ];

  private "addAcademies" = [
    {
      "id": 0,
    }
  ];

  private "addCourses" = [
    {
      "id": 0,
    }
  ];

  private "addCertificates" = [
    {
      "id": 0,
    }
  ];

  // ADD LANGUAGE FIELDS
  private addLanguage() {
    this.addLanguages.push({
      "id": this.langId++,
    });
  }

  // REMOVE LANGUAGE FIELDS
  private removeLanguage() {
    this.addLanguages.length > 1 ? this.addLanguages.pop() : this.addLanguages
  }

  // ADD ACADEMY FIELDS
  private addAcademy() {
    this.addAcademies.push({
      "id": this.academyId++,
    });
  }

  // REMOVE ACADEMY FIELDS
  private removeAcademy() {
    console.log('remove academy');
    this.addAcademies.length > 1 ? this.addAcademies.pop() : this.addAcademies
  }

  // ADD LANGUAGE FIELDS
  private addCourse() {
    this.addCourses.push({
      "id": this.courseId++,
    });
  }

  // REMOVE LANGUAGE FIELDS
  private removeCourse() {
    this.addCourses.length > 1 ? this.addCourses.pop() : this.addCourses
  }

  // ADD CERTIFICATE FIELDS
  private addCertificate() {
    this.addCertificates.push({
      "id": this.certificateId++,
    });
  }

  // ADD CERTIFICATE FIELDS
  private removeCertificate() {
    this.addCertificates.length > 1 ? this.addCertificates.pop() : this.addCertificates
  }

  // DATE FORMAT CONVERTER
  private convertDate(inputFormat) {
    function pad(s) { return (s < 10) ? '0' + s : s; }
    let d = new Date(inputFormat);
    return [d.getFullYear(), pad(d.getMonth()+1), pad(d.getDate())].join('-');
  }

  // FORM SUBMIT (EXTRACT METHODS...)
  private onSubmit(personalDetails: NgForm) {
    { this.submitted = true; }
    console.log(this, personalDetails, personalDetails.value);

    // COLMPOSE LANGUAGES ARRAY
    let languages = [];
    if (this.langId === 1) {
      languages.push(
        {
          "title": personalDetails.value["lang_" + (this.langId - 1)],
          "level": personalDetails.value["langLevel_" + (this.langId - 1)]
        }
      )
    } else if (this.langId > 1) {
      for (let i = 0; i < this.langId; i++) {
        languages.push(
          {
            "title": personalDetails.value["lang_" + i],
            "level": personalDetails.value["langLevel_" + i]
          }
        )
      }
    }

    // COMPOSE CERTIFICATIONS ARRAY
    let certifications = [];
    if (this.certificateId === 1) {
      certifications.push(
        {
          "title": personalDetails.value["certificate_" + (this.certificateId - 1)] === ""
            ? null : personalDetails.value["certificate_" + (this.certificateId - 1)],
          "startDate": personalDetails.value["certStartDate_" + (this.certificateId - 1)] === ""
            ? null : this.convertDate(personalDetails.value["certStartDate_" + (this.certificateId - 1)]),
          "endDate": personalDetails.value["certEndDate_" + (this.certificateId - 1)] === ""
            ? null : this.convertDate(personalDetails.value["certEndDate_" + (this.certificateId - 1)])
        }
      )
    } else if (this.certificateId > 1) {
      for (let i = 0; i < this.certificateId; i++) {
        certifications.push(
          {
            "title": personalDetails.value["certificate_" + i] === ""
              ? null : personalDetails.value["certificate_" + i],
            "startDate": personalDetails.value["certStartDate_" + i] === ""
              ? null : this.convertDate(personalDetails.value["certStartDate_" + i]),
            "endDate": personalDetails.value["certEndDate_" + i] === ""
              ? null : this.convertDate(personalDetails.value["certEndDate_" + i])
          }
        )
      }
    }

    // COMPOSE COURSES ARRAY
    let courses = [];
    if (this.courseId === 1) {
      courses.push(
        {
          "title": personalDetails.value["course_" + (this.courseId - 1)] === ""
            ? null : personalDetails.value["course_" + (this.courseId - 1)],
          "startDate": personalDetails.value["courseStartDate_" + (this.courseId - 1)] === ""
            ? null : this.convertDate(personalDetails.value["courseStartDate_" + (this.courseId - 1)]),
          "endDate": personalDetails.value["courseEndDate_" + (this.courseId - 1)] === ""
            ? null : this.convertDate(personalDetails.value["courseEndDate_" + (this.courseId - 1)])
        }
      )
    } else if (this.courseId > 1) {
      for (let i = 0; i < this.courseId; i++) {
        courses.push(
          {
            "title": personalDetails.value["course_" + i] === ""
              ? null : personalDetails.value["course_" + i],
            "startDate": personalDetails.value["courseStartDate_" + i] === ""
              ? null : this.convertDate(personalDetails.value["courseStartDate_" + i]),
            "endDate": personalDetails.value["courseEndDate_" + i] === ""
              ? null : this.convertDate(personalDetails.value["courseEndDate_" + i])
          }
        )
      }
    }

    // COMPOSE ACADEMIES ARRAY (MANDATORY)
    let academies = [];
    if (this.academyId === 1) {
      academies.push(
        {
          "title": personalDetails.value["academy_" + (this.academyId - 1)],
          "institution": personalDetails.value["academyInstitution_" + (this.academyId - 1)],
          "startDate": personalDetails.value["academyStartDate_" + (this.academyId - 1)] === ''
            ? null : this.convertDate(personalDetails.value["academyStartDate_" + (this.academyId - 1)]),
          "endDate": this.convertDate(personalDetails.value["academyEndDate_" + (this.academyId - 1)])
        }
      )
    } else if (this.academyId > 1) {
      for (let i = 0; i < this.academyId; i++) {
        academies.push(
          {
            "title": personalDetails.value["academy_" + i],
            "institution": personalDetails.value["academyInstitution_" + i],
            "startDate": personalDetails.value["academyStartDate_" + i] === ''
              ? null : this.convertDate(personalDetails.value["academyStartDate_" + i]),
            "endDate": this.convertDate(personalDetails.value["academyEndDate_" + i])
          }
        )
      }
    }
    let personalDetailsModel = {
      "academies": academies,
      "certifications": certifications,
      "courses": courses,
      "languages": languages,
      "name": personalDetails.value.name,
      "surname": personalDetails.value.surname,
      "birthDate": this.convertDate(personalDetails.value.birthDate),
      "phoneNumber": personalDetails.value.phoneNumber,
      "email": personalDetails.value.email,
      "nationality": personalDetails.value.nationality,
      "role": personalDetails.value.role,
      "description": personalDetails.value.description === "" ? null : personalDetails.value.description
    }

    localStorage.setItem('personalDetailsModel', JSON.stringify(personalDetailsModel));
    // return this.personalDetailsService.submitRequest(personalDetailsModel)
    // .subscribe(
    //   data => {
    //     console.log('suscess', data);
    //     this.submitted = true;
    //   },
    //   err => {
    //     console.log('errorrrrr');
    //     console.log(err.error);
    //   }
    // );
  }
}
