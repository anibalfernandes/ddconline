import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConsultantsList } from '../../consultant-list/store/consultant-list.interface';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin':  'http://127.0.0.1:4200',
    'Access-Control-Allow-Methods': 'GET'
  })
};

@Injectable()
export class ConsultantsService {

  private url: string = "http://localhost:8080/consultants";
  constructor(private http: HttpClient) { }

  /** fetch details of the selected consultant from the server */
  getConsultantDetails(consultantId): Observable<ConsultantsList> {
    console.log("entered service");
    return this.http.get<ConsultantsList>(this.url+'/'+consultantId, httpOptions);
  }

}
