import { Component } from '@angular/core';
import { Injectable } from '@angular/core';
import { OnInit } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { NgForm } from '@angular/forms';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { ConsultantsList } from './store/consultants.interface';
import { ActivatedRoute } from '@angular/router';
import { ConsultantsService } from './services/consultants.service';

@Component({
  selector: 'consultants-section',
  templateUrl: 'consultants.section.html',
  styleUrls: ['consultants.section.scss']
})

export class ConsultantsSection {
  private url: string = "http://localhost:8080/consultants";
  private consultantsData;
  constructor(
    private route: ActivatedRoute,
    private http: HttpClient,
    private consultantsService: ConsultantsService
  ) { }

  // FORM SUBMIT (EXTRACT METHODS...)
  ngOnInit() {

    const consultantId = +this.route.snapshot.paramMap.get('consultantId');
    console.log('consultantId:'+consultantId);

    return this.consultantsService.getConsultantDetails(consultantId)
   .subscribe(data => {
      this.consultantsData = data;
      console.log("consultant data:"+JSON.stringify(data));
    });
  }

  private extractData(res: HttpResponse<any> | any) {
    let body = res.json();
    return body.data || {};
  }

  private handleErrorObservable(error: HttpResponse<any> | any) {
    console.error(error.message || error);
    return Observable.throw(error.message || error);
  }

  private handleErrorPromise(error: HttpResponse<any> | any) {
    console.error(error.message || error);
    return Promise.reject(error.message || error);
  }
}
