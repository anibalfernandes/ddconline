export interface ConsultantsList {
  _embedded: {
    consultants: [
      consultants
    ]
  },
  _links: _links;
  page: page;
}

export interface consultants {
  academies: [ academies ];
  certifications: [
    certifications
  ];
  courses: [
    courses
  ];
  languages: [
    languages
  ],
  name: string;
  surname: string;
  birthDate: string;
  phoneNumber: string;
  email: string;
  nationality: string;
  role: number;
  description: null;
  _links: _links;
}

export interface academies {
  title: string;
  startDate: string;
  endDate: string;
  institution: string
}

export interface certifications {
  title: string;
  startDate: string;
  endDate: string;
}

export interface courses {
  title: string;
  startDate: string;
  endDate: string;
}

export interface languages {
  title: number;
  level: number;
}

export interface self {
  href: string;
  templated: boolean;
}

export interface profile {
  href: string;
}

export interface _links {
  self: self;
  profile: profile;
}

export interface page {
  size: number;
  totalElements: number;
  totalPages: number;
  number: number;
}
