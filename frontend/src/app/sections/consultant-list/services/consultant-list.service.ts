import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConsultantsList } from '../../consultant-list/store/consultant-list.interface';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin':  'http://127.0.0.1:4200',
    'Access-Control-Allow-Methods': 'GET'
  })
};

@Injectable()
export class ConsultantListService {

  private url: string = "http://localhost:8080/consultants";
  constructor(private http: HttpClient) { }

  /** fetch all consultant from the server */
  getAllConsultants(): Observable<ConsultantsList> {
  
    return this.http.get<ConsultantsList>(this.url, httpOptions);
  }

}
