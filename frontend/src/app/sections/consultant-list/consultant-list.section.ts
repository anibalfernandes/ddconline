import { Component } from '@angular/core';
import { Injectable } from '@angular/core';
import { OnInit } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { NgForm } from '@angular/forms';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { ConsultantsList } from './store/consultant-list.interface';
import { ConsultantListService } from './services/consultant-list.service';

@Component({
  selector: 'consultant-list-section',
  templateUrl: 'consultant-list.section.html',
  styleUrls: ['consultant-list.section.scss']
})

export class ConsultantListSection {
  private url: string = "http://localhost:8080/consultants";
  private consultantsData;
  constructor(private http: HttpClient,
      private consultantListService: ConsultantListService) { }

  // FORM SUBMIT (EXTRACT METHODS...)
  ngOnInit() {
  
    this.getConsultantsList();
  }

  private getConsultantsList() {
    return this.consultantListService.getAllConsultants().subscribe(
      data => {
        this.consultantsData = data;
        console.log("data:"+JSON.stringify(data));
      },
      err => {
        console.log("Error occured.")
      }
    );
  }

  private extractData(res: HttpResponse<any> | any) {
    let body = res.json();
    return body.data || {};
  }

  private handleErrorObservable(error: HttpResponse<any> | any) {
    console.error(error.message || error);
    return Observable.throw(error.message || error);
  }

  private handleErrorPromise(error: HttpResponse<any> | any) {
    console.error(error.message || error);
    return Promise.reject(error.message || error);
  }
}
