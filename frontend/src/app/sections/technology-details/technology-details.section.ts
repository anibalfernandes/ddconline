import { Component } from '@angular/core';
import { Injectable } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgForm } from '@angular/forms';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { TechnologyDetails } from './store/technology-details.interface';
import { TechnologyDetailsService } from './services/technology-details.service';

@Component({
  selector: 'technology-details-section',
  templateUrl: 'technology-details.section.html',
  styleUrls: ['technology-details.section.scss']
})

export class TechnologyDetailsSection {
  private url: string = "http://localhost:8080/consultants";
  private profileId: number = 1;
  private devId: number = 1;

  constructor(private http: HttpClient,
    private technologyDetailsService: TechnologyDetailsService) { }

  submitted = false;

  private "addProfiles" = [
    {
      "id": 0,
    }
  ];
  private "addDevelopments" = [
    {
      "id": 0,
    }
  ];

  // ADD ACADEMY FIELDS
  private addProfile() {
    this.addProfiles.push({
      "id": this.profileId++,
    });
  }

  // REMOVE ACADEMY FIELDS
  private removeProfile() {
    console.log('remove profile');
    this.addProfiles.length > 1 ? this.addProfiles.pop() : this.addProfiles
  }

  
  // ADD ACADEMY FIELDS
  private addDev() {
    this.addDevelopments.push({
      "id": this.devId++,
    });
  }

  // REMOVE ACADEMY FIELDS
  private removeDev() {
    console.log('remove dev');
    this.addDevelopments.length > 1 ? this.addDevelopments.pop() : this.addDevelopments
  }

  // FORM SUBMIT (EXTRACT METHODS...)
  private onSubmit(technologyDetails: NgForm) {
    { this.submitted = true; }
    console.log(this, technologyDetails, technologyDetails.value);

    // COMPOSE PROFILES ARRAY (MANDATORY)
    let profiles = [];
    if (this.profileId === 1) {
      profiles.push(
        {
          "profile": technologyDetails.value["profile_" + (this.profileId - 1)],
          "level": technologyDetails.value["exp_" + (this.profileId - 1)]
        }
      )
    } else if (this.profileId > 1) {
      for (let i = 0; i < this.profileId; i++) {
        profiles.push(
          {
            "profile": technologyDetails.value["profile_" + i],
            "level": technologyDetails.value["exp_" + i]
          }
        )
      }
    }

    let devs = [];
    if (this.devId === 1) {
      devs.push(
        {
          "dev": technologyDetails.value["dev_" + (this.devId - 1)],
          "level": technologyDetails.value["level_" + (this.devId - 1)],
          "expTime": technologyDetails.value["expTime_" + (this.devId - 1)]
        }
      )
    } else if (this.devId > 1) {
      for (let i = 0; i < this.devId; i++) {
        devs.push(
          {
            "dev": technologyDetails.value["dev_" + i],
            "level": technologyDetails.value["level_" + i],
            "expTime": technologyDetails.value["expTime_" + i]
          }
        )
      }
    }


    let technologyDetailsModel = {
      "name": technologyDetails.value.name,
      "surname": technologyDetails.value.surname,
      "phoneNumber": technologyDetails.value.phoneNumber,
      "email": technologyDetails.value.email,
      "nationality": technologyDetails.value.nationality,
      "role": technologyDetails.value.role,
      "description": technologyDetails.value.description === "" ? null : technologyDetails.value.description
    }


    return this.technologyDetailsService.submitRequest(technologyDetailsModel)
      .subscribe(
      data => {
        console.log('suscess', data);
        this.submitted = true;
      },
      err => {
        console.log('errorrrrr');
        console.log(err.error);
      }
      );
  }
}
