import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { TechnologyDetails } from '../store/technology-details.interface';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': 'http://127.0.0.1:4200',
    'Access-Control-Allow-Methods': 'POST',
    'Access-Control-Allow-Headers': 'Authorization'
  })
};

@Injectable()
export class TechnologyDetailsService {

  private url: string = "http://localhost:8080/consultants";
  constructor(private http: HttpClient) { }

  /** submit consultant details to the server */
  submitRequest(technologyDetailsModel): Observable<TechnologyDetails> {
    return this.http.post<TechnologyDetails>(this.url, technologyDetailsModel, httpOptions);
  }

}
