import { Component, Input } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'input-email',
  templateUrl: 'input-email.component.html',
  styleUrls: ['input-email.component.scss']
})
export class InputEmail {
  @Input() placeholder: string;
  @Input() required: string;
  email = new FormControl('', [Validators.required, Validators.email]);
  getErrorMessage() {
    return this.email.hasError('required') ? 'You must enter a value' :
      this.email.hasError('pattern') ? 'Please enter a valid email' :
        '';
  }
}