import { Component, Input } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'input-text',
  templateUrl: 'input-text.component.html',
  styleUrls: ['input-text.component.scss']
})
export class InputText {
  @Input() placeholder: string;
  @Input() required: string;
  @Input() type: string;

  text = new FormControl('', [Validators.required]);

  getErrorMessage() {
    return this.text.hasError('required') ? 'You must enter a value' : '';
  }
}
