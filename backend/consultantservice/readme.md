# Description

The consultant service is a SpringBoot application that will serve as a REST endpoint in order to expose operations for creating, updating and deleting consultants.

# Prerequisites

Sql server should be installed.

The DataBase "Agap2DdcOnline" should be created inside the Sql server instance.


# How to run it

To run the springboot application, follow the next steps:

   1)Navigate to ddconline/backend/consultantservice

   2)Type in the command "./gradlew build"

   3)Type in the command "java -jar build/libs/consultantservice-1.0.jar"

   4)Open a web browser

   5)Check that you see the Rest end points operations by looking into "http://localhost:8080"


# How to manage the Database

The DataBase is created before the application starts. The DataBase is created by using a tool called "LiquiBase".
Everytime we need to change our DataBase for any reason, we will do it following these steps:
  
    - Create a new file inside the path "/db/changelog"
    - The new file should be named "db.changelog-1.X.yaml", being X a sequence number based in the previous one.
    - Add all the changes needed in the DataBase inside the file created in the step before. 
    - Add a reference to the newly created file inside the file "db.changelog-master.yaml"

For more information regarding Liquibase, go to: http://www.liquibase.org/documentation/index.html


# Errors

- When trying to send an operation to the rest api, an exception is returned:

     This could happen because the rest api uses sql server as the DataBase to store the consultants information. sql server needs to be running in port 1433 in order for the application to be able to store information.
     The connection to the DataBase is configured inside the file "application.properties".

