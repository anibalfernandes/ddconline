package agap2.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.time.LocalDate;

@Embeddable
@ApiModel(description = "Course details.")
public class Course {

    @Column(name="title", columnDefinition = "nvarchar")
    @ApiModelProperty(
            notes = "Title of the course.",
            example = "Java SE 7 Programming 101")
    private String title;

    @Column(name = "startdate")
    @ApiModelProperty(
            notes = "Start date.",
            example = "2018-01-19")
    private LocalDate startDate;

    @Column(name = "enddate")
    @ApiModelProperty(
            notes = "End date.",
            example = "2018-01-19")
    private LocalDate endDate;

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
