package agap2.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Min;

@Embeddable
@ApiModel(description = "Profile details.")
public class Profile {

    @Column(name="profile", columnDefinition = "nvarchar")
    @NotEmpty
    @ApiModelProperty(
            notes = "Profile title / description. Must not be empty.",
            example = "Development")
    private String profile;

    @Min(1)
    @ApiModelProperty(
            notes = "Level: 1 - Academic; 2 - Professional.",
            example = "1")
    int level;

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}

