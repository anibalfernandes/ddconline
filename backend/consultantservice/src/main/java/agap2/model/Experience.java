package agap2.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Min;

@Embeddable
@ApiModel(description = "Acquired experience details.")
public class Experience {

    @Column(name="title", columnDefinition = "nvarchar")
    @NotEmpty
    @ApiModelProperty(
            notes = "Title / description. Must not be empty.",
            example = "Confluence")
    private String title;

    @Min(1)
    @ApiModelProperty(
            notes = "Level: 1 - Junior; 2 - Intermediate; 3 - Independent; 4 - Specialist.",
            example = "1")
    int level;

    @Column(name="experience", columnDefinition = "nvarchar")
    @NotEmpty
    @ApiModelProperty(
            notes = "Experience time. Examples: '1 year'; '2 months'; '5 years 3 months'. Must not be empty.",
            example = "1 year")
    private String experience;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }
}

