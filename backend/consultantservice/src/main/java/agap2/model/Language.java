package agap2.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Embeddable;
import javax.validation.constraints.Min;

@Embeddable
@ApiModel(description = "Languages' skills details.")
public class Language {

    @Min(1)
    @ApiModelProperty(
            notes = "Language identifier.",
            example = "1")
    int title;

    @Min(1)
    @ApiModelProperty(
            notes = "Language level: 1 - Basic; 2 - Training; 3 - Professional; 4 - Bilingual; 5 - Mother tongue.",
            example = "1")
    int level;

    public int getTitle() {
        return title;
    }

    public void setTitle(int title) {
        this.title = title;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
