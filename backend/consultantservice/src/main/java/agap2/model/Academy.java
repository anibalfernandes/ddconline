package agap2.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.time.LocalDate;

@Embeddable
@ApiModel(description = "Academic institution details.")
public class Academy {

    @Column(name="title", columnDefinition = "nvarchar")
    @NotEmpty
    @ApiModelProperty(
            notes = "Title of the degree. Must not be empty.",
            example = "Master of Science in Computer Engineering")
    private String title;

    @Column(name = "startdate")
    @ApiModelProperty(
            notes = "Start date.",
            example = "2018-01-19")
    private LocalDate startDate;

    @Column(name = "enddate")
    @ApiModelProperty(
            notes = "End date.",
            example = "2018-01-19")
    private LocalDate endDate;

    @Column(name="institution", columnDefinition = "nvarchar")
    @NotEmpty
    @ApiModelProperty(
            notes = "Name of the academic institution. Must not be empty.",
            example = "Instituto Superior Técnico (IST)")
    private String institution;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }
}

