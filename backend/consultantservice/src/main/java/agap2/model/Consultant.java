package agap2.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Set;

@Entity
@ApiModel(description = "Consultant detailed sheet.")
public class Consultant {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    @Column(name = "consultantid")
    @ApiModelProperty(
            notes = "Unique identifier of the consultant.",
            example = "123")
    private long consultantId;

    @ElementCollection
    @CollectionTable(name="Academy",joinColumns = @JoinColumn(name="consultantid"))
    @NotNull
    @ApiModelProperty(
            notes = "Academic institutions: should have at least 1 item.")
    @Size(min = 1, message = "Should have at least 1 item.")
    private Set<Academy> academies;

    @ElementCollection
    @CollectionTable(name = "Certification",joinColumns = @JoinColumn(name = "consultantid"))
    @ApiModelProperty(notes = "Professional certifications acquired by consultant.")
    private Set<Certification> certifications;

    @ElementCollection
    @CollectionTable(name = "Course",joinColumns = @JoinColumn(name = "consultantid"))
    @ApiModelProperty(notes = "Courses taken by consultant.")
    private Set<Course> courses;

    @ElementCollection
    @CollectionTable(name = "Language",joinColumns = @JoinColumn(name = "consultantid"))
    @ApiModelProperty(notes = "Consultant's languages skills.")
    private Set<Language> languages;

    @ElementCollection
    @CollectionTable(name = "Skill",joinColumns = @JoinColumn(name = "consultantid"))
    @Size(min = 1)
    @ApiModelProperty(notes = "Consultant's skills.")
    private Set<Skill> skills;

    @Column(name = "name", columnDefinition = "nvarchar")
    @NotEmpty
    @ApiModelProperty(
            notes = "First name of the consultant. Must not be empty.",
            example = "John")
    private String name;

    @Column(name = "surname", columnDefinition = "nvarchar")
    @NotEmpty
    @ApiModelProperty(
            notes = "Last name of the consultant. Must not be empty.",
            example = "Doe")
    private String surname;

    @Column(name = "birthdate")
    @NotNull
    @ApiModelProperty(
            notes = "Birth date of consultant. Must not be empty.",
            example = "1975-01-19")
    private LocalDate birthDate;

    @Column(name = "phonenumber", columnDefinition = "nvarchar")
    @NotEmpty
    @ApiModelProperty(
            notes = "Phone number of consultant. Must not be empty.",
            example = "+310611666789")
    private String phoneNumber;

    @Column(name = "email", columnDefinition = "nvarchar")
    @NotEmpty
    @ApiModelProperty(
            notes = "E-mail of consultant. Must not be empty.",
            example = "john.doe@gmail.com")
    private String email;

    @Column(name = "nationality", columnDefinition = "nvarchar")
    @NotEmpty
    @ApiModelProperty(
            notes = "Nationality of consultant. Must not be empty.",
            example = "Scottish")
    private String nationality;

    @Min(1)
    @ApiModelProperty(
            notes = "Role identifier of consultant. Must not be empty. Minimum is 1.",
            example = "1")
    private int role;

    @Column(name = "description", columnDefinition = "nvarchar")
    @ApiModelProperty(
            notes = "Description of consultant.",
            example = "Short description of consultant's career path.")
    private String description;

    @ElementCollection
    @CollectionTable(name="Profile",joinColumns = @JoinColumn(name="consultantid"))
    @Size(min = 1, message = "At least 1 profile is required.")
    @ApiModelProperty(
            notes = "Past and present profiles performed by consultant.")
    private Set<Profile> profiles;

    @ElementCollection
    @CollectionTable(name="Technology",joinColumns = @JoinColumn(name="consultantid"))
    @ApiModelProperty(notes = "Consultant's technological skills.")
    private Set<Experience> technologies;

    @ElementCollection
    @CollectionTable(name="Methodology",joinColumns = @JoinColumn(name="consultantid"))
    @ApiModelProperty(notes = "Consultant's methodology skills.")
    private Set<Experience> methodologies;

    @ElementCollection
    @CollectionTable(name="Database",joinColumns = @JoinColumn(name="consultantid"))
    @ApiModelProperty(notes = "Consultant's database skills.")
    private Set<Experience> databases;

    @ElementCollection
    @CollectionTable(name="Architecture",joinColumns = @JoinColumn(name="consultantid"))
    @ApiModelProperty(notes = "Consultant's Architecture & Protocol skills.")
    private Set<Experience> architectures;

    @ElementCollection
    @CollectionTable(name="Operatingsystem",joinColumns = @JoinColumn(name="consultantid"))
    @ApiModelProperty(notes = "Consultant's Operating Systems skills.")
    private Set<Experience> operatingSystems;

    @ElementCollection
    @CollectionTable(name="Project",joinColumns = @JoinColumn(name="consultantid"))
    @ApiModelProperty(notes = "Past and present projects performed by consultant.")
    private Set<Project> projects;

    public Set<Profile> getProfiles() {
        return profiles;
    }

    public void setProfiles(Set<Profile> profiles) {
        this.profiles = profiles;
    }

    public Set<Experience> getTechnologies() {
        return technologies;
    }

    public void setTechnologies(Set<Experience> technologies) {
        this.technologies = technologies;
    }

    public Set<Experience> getMethodologies() {
        return methodologies;
    }

    public void setMethodologies(Set<Experience> methodologies) {
        this.methodologies = methodologies;
    }

    public Set<Experience> getDatabases() {
        return databases;
    }

    public void setDatabases(Set<Experience> databases) {
        this.databases = databases;
    }

    public Set<Experience> getArchitectures() {
        return architectures;
    }

    public void setArchitectures(Set<Experience> architectures) {
        this.architectures = architectures;
    }

    public Set<Experience> getOperatingSystems() {
        return operatingSystems;
    }

    public void setOperatingSystems(Set<Experience> operatingSystems) {
        this.operatingSystems = operatingSystems;
    }

    public Set<Project> getProjects() {
        return projects;
    }

    public void setProjects(Set<Project> projects) {
        this.projects = projects;
    }

    public long getConsultantId() {
        return consultantId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Academy> getAcademies() {
        return academies;
    }

    public void setAcademies(Set<Academy> academies) {
        this.academies = academies;
    }

    public Set<Certification> getCertifications() {
        return certifications;
    }

    public void setCertifications(Set<Certification> certifications) {
        this.certifications = certifications;
    }

    public Set<Course> getCourses() {
        return courses;
    }

    public void setCourses(Set<Course> courses) {
        this.courses = courses;
    }

    public Set<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(Set<Language> languages) {
        this.languages = languages;
    }

    public Set<Skill> getSkills() {
        return skills;
    }

    public void setSkills(Set<Skill> skills) {
        this.skills = skills;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }
}
