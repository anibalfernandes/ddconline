package agap2.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Embeddable;
import javax.validation.constraints.Min;

@Embeddable
@ApiModel(description = "Skill details.")
public class Skill {

    @Min(1)
    @ApiModelProperty(
            notes = "Skill title.",
            example = "1")
    int title;

    @Min(1)
    @ApiModelProperty(
            notes = "Skill level.",
            example = "1")
    int level;

    public int getTitle() {
        return title;
    }

    public void setTitle(int title) {
        this.title = title;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
