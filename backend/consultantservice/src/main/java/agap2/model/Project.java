package agap2.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.time.LocalDate;

@Embeddable
@ApiModel(description = "Project details.")
public class Project {

    @Column(name="company", columnDefinition = "nvarchar")
    @NotEmpty
    @ApiModelProperty(
            notes = "Company name. Must not be empty.",
            example = "Agap2IT")
    private String company;

    @Column(name="function", columnDefinition = "nvarchar")
    @NotEmpty
    @ApiModelProperty(
            notes = "Title of function carried out. Must not be empty.",
            example = "Software Development")
    private String function;

    @Column(name = "startdate")
    @NotEmpty
    @ApiModelProperty(
            notes = "Start date. Must not be empty.",
            example = "2015-01-19")
    private LocalDate startDate;

    @Column(name = "enddate")
    @ApiModelProperty(
            notes = "End date.",
            example = "2018-01-19")
    private LocalDate endDate;

    @Column(name="description", columnDefinition = "nvarchar")
    @NotEmpty
    @ApiModelProperty(
            notes = "Description of function carried out. Must not be empty.",
            example = "Mobile App Development for financial company X.")
    private String description;

    @Column(name="techstack", columnDefinition = "nvarchar")
    @NotEmpty
    @ApiModelProperty(
            notes = "Technological stack used. Must not be empty. Example: 'JAVA EE 7; Python 2.7; Angular 4'",
            example = "JAVA EE 7")
    private String techstack;

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTechstack() {
        return techstack;
    }

    public void setTechstack(String techstack) {
        this.techstack = techstack;
    }
}

