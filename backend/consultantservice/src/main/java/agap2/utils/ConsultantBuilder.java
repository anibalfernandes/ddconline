package agap2.utils;

import agap2.model.*;

import java.time.LocalDate;
import java.util.Set;

public class ConsultantBuilder {

    private Consultant consultant;

    public ConsultantBuilder(){
        consultant = new Consultant();
    }

    public ConsultantBuilder profiles(Set<Profile> profiles){
        consultant.setProfiles(profiles);
        return this;
    }

    public ConsultantBuilder technologies(Set<Experience> technologies){
        consultant.setTechnologies(technologies);
        return this;
    }

    public ConsultantBuilder methodologies(Set<Experience> methodologies){
        consultant.setMethodologies(methodologies);
        return this;
    }

    public ConsultantBuilder databases(Set<Experience> databases){
        consultant.setDatabases(databases);
        return this;
    }

    public ConsultantBuilder architectures(Set<Experience> architectures){
        consultant.setArchitectures(architectures);
        return this;
    }

    public ConsultantBuilder operatingSystems(Set<Experience> operatingSystems){
        consultant.setOperatingSystems(operatingSystems);
        return this;
    }

    public ConsultantBuilder projects(Set<Project> projects){
        consultant.setProjects(projects);
        return this;
    }

    public ConsultantBuilder birthDate(LocalDate birthDate) {
        consultant.setBirthDate(birthDate);
        return this;
    }

    public ConsultantBuilder academies(Set<Academy> academies) {
        consultant.setAcademies(academies);
        return this;
    }

    public ConsultantBuilder nationality(String nationality) {
        consultant.setNationality(nationality);
        return this;
    }

    public ConsultantBuilder email(String email) {
        consultant.setEmail(email);
        return this;
    }

    public ConsultantBuilder name(String name) {
        consultant.setName(name);
        return this;
    }

    public ConsultantBuilder surname(String surname) {
        consultant.setSurname(surname);
        return this;
    }

    public ConsultantBuilder phoneNumber(String phoneNumber) {
        consultant.setPhoneNumber(phoneNumber);
        return this;
    }

    public ConsultantBuilder certifications(Set<Certification> certifications) {
        consultant.setCertifications(certifications);
        return this;
    }

    public ConsultantBuilder courses(Set<Course> courses) {
        consultant.setCourses(courses);
        return this;
    }

    public ConsultantBuilder languages(Set<Language> languages) {
        consultant.setLanguages(languages);
        return this;
    }

    public ConsultantBuilder skills(Set<Skill> skills) {
        consultant.setSkills(skills);
        return this;
    }

    public ConsultantBuilder role(int role) {
        consultant.setRole(role);
        return this;
    }

    public ConsultantBuilder description(String description) {
        consultant.setDescription(description);
        return this;
    }

    public Consultant build(){
        return consultant;
    }
}
