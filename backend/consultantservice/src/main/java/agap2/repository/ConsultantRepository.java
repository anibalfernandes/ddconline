package agap2.repository;

import agap2.model.Consultant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@RepositoryRestResource(path = "consultants")
@CrossOrigin
@Api(tags = "Consultant Entity")
public interface ConsultantRepository extends PagingAndSortingRepository<Consultant, Long> {

    @ApiOperation("Retrieve all consultants.")
    Page<Consultant> findAll(Pageable pageable);

    @ApiOperation("Save consultant.")
    <S extends Consultant> S save(S entity);

    @ApiOperation("Delete consultant.")
    void delete(Consultant entity);

    @ApiOperation("Retrieve consultant by identifier.")
    Consultant findOne(Long id);

    @ApiOperation("Save consultants.")
    <S extends Consultant> Iterable<S> save(Iterable<S> entities);

}
