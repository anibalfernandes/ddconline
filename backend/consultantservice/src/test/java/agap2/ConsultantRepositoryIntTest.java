package agap2;

import agap2.model.*;
import agap2.utils.ConsultantBuilder;
import agap2.utils.LocalDateAdapter;
import com.google.gson.GsonBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
public class ConsultantRepositoryIntTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void testCreateValidConsultant() throws Exception {
        Consultant consultant = getCompleteConsultant();
        String consultantInJSON = getJsonString(consultant);

        mvc.perform(
                MockMvcRequestBuilders.post("/consultants")
                        .contentType(MediaType.APPLICATION_JSON).content(consultantInJSON))
                .andExpect(MockMvcResultMatchers.status().isCreated());


        String result = mvc.perform(
                MockMvcRequestBuilders.get("/consultants")
                        .contentType(MediaType.APPLICATION_JSON).content("{\"page\": 1}"))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();

        assert result.contains(consultant.getName());
        assert result.contains(consultant.getDescription());
        assert result.contains(consultant.getSurname());
        assert result.contains(consultant.getEmail());
    }

    @Test
    public void testCreateInvalidConsultant() throws Exception{
        Consultant consultant = getCompleteConsultant();
        consultant.setName(null);
        String consultantInJSON = getJsonString(consultant);

        mvc.perform(
                MockMvcRequestBuilders.post("/consultants")
                        .contentType(MediaType.APPLICATION_JSON).content(consultantInJSON))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    private String getJsonString(Consultant consultant) {
        return new GsonBuilder().registerTypeAdapter(LocalDate.class, new LocalDateAdapter()).create().toJson(consultant);
    }

    private Consultant getCompleteConsultant() {
        Set<Academy> academies = new HashSet<>();
        Academy academy = new Academy();
        academy.setTitle("Academy A");
        academy.setStartDate(LocalDate.of(2016,1,2));
        academy.setEndDate(LocalDate.of(2017,2,3));
        academy.setInstitution("Institution Q");
        academies.add(academy);

        Set<Certification> certifications = new HashSet<>();
        Certification certification = new Certification();
        certification.setTitle("certification X");
        certification.setStartDate(LocalDate.of(2014,3,6));
        certification.setEndDate(LocalDate.of(2015,4,5));
        certifications.add(certification);

        Set<Course> courses = new HashSet<>();
        Course course = new Course();
        course.setTitle("Course XPTO");
        course.setStartDate(LocalDate.of(2016,3,6));
        course.setEndDate(LocalDate.of(2017,4,5));
        courses.add(course);

        Set<Language> languages = new HashSet<>();
        Language language = new Language();
        language.setLevel(1);
        language.setTitle(3);
        languages.add(language);

        Set<Skill> skills = new HashSet<>();
        Skill skill = new Skill();
        skill.setLevel(2);
        skill.setTitle(1);
        skills.add(skill);

        String description = "This is John Doe's consultant file description.";

        Set<Profile> profiles = new HashSet<>();
        Profile profile = new Profile();
        profile.setLevel(1);
        profile.setProfile("software development");
        profiles.add(profile);
        Set<Experience> techs = new HashSet<>();
        Experience tech = new Experience();
        tech.setExperience("qwe");
        tech.setLevel(1);
        tech.setTitle("AAA");
        techs.add(tech);
        Set<Experience> methodologies = new HashSet<>();
        methodologies.add(tech);
        Set<Experience> dbs = new HashSet<>();
        dbs.add(tech);
        Set<Experience> architecs = new HashSet<>();
        architecs.add(tech);
        Set<Experience> oss = new HashSet<>();
        oss.add(tech);
        Set<Project> projects = new HashSet<>();
        Project project = new Project();
        project.setCompany("company");
        project.setDescription("des");
        project.setEndDate(LocalDate.of(2014,3,6));
        project.setStartDate(LocalDate.of(2014,3,6));
        project.setFunction("func");
        project.setTechstack("tech");
        projects.add(project);

        return new ConsultantBuilder().
                profiles(profiles).
                technologies(techs).
                methodologies(methodologies).
                databases(dbs).
                architectures(architecs).
                operatingSystems(oss).
                projects(projects).
                academies(academies).
                certifications(certifications).
                courses(courses).
                languages(languages).
                skills(skills).
                name("Test").
                surname("Dummy").
                birthDate(LocalDate.of(1980, 1, 1)).
                phoneNumber("000555777").
                email("test.dummy@unknow.nl").
                nationality("Alien").
                role(1).
                description(description).build();
    }
}